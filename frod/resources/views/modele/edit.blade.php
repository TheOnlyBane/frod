@extends('layouts.app')

@section('content')

<div class="formFROD card">
    <h1 class="titreFormFROD">Modification d'un modele</h1>
    <form method="POST" action="/modeles/{{$modele->id}}">
        @method('PATCH')
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="contentForm">
            <div class="form-group">
                <input class="form-control"  type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value={{$modele->nom}} />
            </div>
            <div class="form-group">
                <input class="form-control"  type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$modele->prix}} />
            </div>
            <div class="form-group">
                <textarea class="form-control"  {{$errors->has('description') ? 'is-danger' : ''}} name="description"> {{$modele->description}} </textarea>
            </div>
            <div class="form-group">
                <input class="form-control"  type="text" {{$errors->has('transmission') ? 'is-danger' : ''}} name="transmission" value={{$modele->transmission}} />
            </div>
            <div class="form-group">
                <input class="form-control"  type="number" min="1800" max="2100" step="1" {{$errors->has('annee') ? 'is-danger' : ''}} name="annee" value={{$modele->annee}} />
            </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
        </div>
    </form>
    <button id="btnModeleDelete" data-nomModele="{{$modele->nom}}" data-idModele="{{$modele->id}}" class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
    <form id="formDeleteModele" method="POST" action="/modeles/{{$modele->id}}">
        @method('DELETE')
        @csrf
    </form>
</div>

@endsection
