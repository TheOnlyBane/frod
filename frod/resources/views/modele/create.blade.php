@extends('layouts.app')

@section('content')

<div class="formFROD card">
    <h1 class="titreFormFROD card-header">Ajout d'un modele</h1>
    <form method="POST" action="/modeles/">
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="contentForm">
            <div class="form-group">
                <input class="form-control" type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" placeholder="Nom" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
            </div>
            <div class="form-group">
                <textarea class="form-control" {{$errors->has('description') ? 'is-danger' : ''}} name="description" placeholder="Description"></textarea>
            </div>
            <div class="form-group">
                <input class="form-control" type="text" {{$errors->has('transmission') ? 'is-danger' : ''}} name="transmission" placeholder="Transmission" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="1800" max="2100" step="1" {{$errors->has('annee') ? 'is-danger' : ''}} name="annee" placeholder="Année" />
            </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
        </div>

    </form>
</div>
@endsection
