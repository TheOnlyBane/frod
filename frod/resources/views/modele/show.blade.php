@extends('layouts.app')

@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Affichage d'un modele</h1>
        <form method="POST" action="/modeles/{{$modele->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="contentForm">
                <div class="form-group">
                    <input class="form-control" readonly="readonly"  type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value={{$modele->nom}} />
                </div>
                <div class="form-group">
                    <input class="form-control" readonly="readonly" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$modele->prix}} />
                </div>
                <div class="form-group">
                    <textarea class="form-control" readonly="readonly" {{$errors->has('description') ? 'is-danger' : ''}} name="description"> {{$modele->description}} </textarea>
                </div>
                <div class="form-group">
                    <input class="form-control" readonly="readonly" type="text" {{$errors->has('transmission') ? 'is-danger' : ''}} name="transmission" value={{$modele->transmission}} />
                </div>
                <div class="form-group">
                    <input class="form-control" readonly="readonly" type="number" min="1800" max="2100" step="1" {{$errors->has('annee') ? 'is-danger' : ''}} name="annee" value={{$modele->annee}} />
                </div>
                <a class="btn btn-primary btnFormFROD" href="/modeles/{{$modele->id}}/edit">Modifier</a>
            </div>
        </form>
    </div>

@endsection
