@extends('layouts.app')

@section('content')
    <div class="table table-hover container-fluid liste card">
        <h1 class="titreFormFROD card-header">Liste de Modeles</h1>
            <table>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Nom</th>
                    <th scope="col">Prix</th>
                    <th scope="col">Description</th>
                    <th scope="col">Transmission</th>
                    <th scope="col">Annee</th>
                </tr>
                @foreach ($modeles as $modele)
                    <tr>
                        <th><a href="/modeles/{{$modele->id}}">{{$modele->id}}</a></td>
                        <td>{{$modele->nom}}</td>
                        <td>{{$modele->prix}}$</td>
                        <td>{{$modele->description}}</td>
                        <td>{{$modele->transmission}}</td>
                        <td>{{$modele->annee}}</td>
                    </tr>
                @endforeach
            </table>
        <a class="btn btn-primary" href="/modeles/create" >Ajouter</a>
    </div>
@endsection
