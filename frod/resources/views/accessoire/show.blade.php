@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Modification d'un accessoire</h1>
            <div class="content">
                <div class="form-group">
                    <input readonly="readonly" class="form-control" type="text" min="2" max="12" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value="{{$accessoire->nom}}" />
                </div>
                <div class="form-group">
                    <input readonly="readonly" class="form-control" type="number" min="1" max="20" step="0.1" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$accessoire->prix}} />
                </div>
                <div class="form-group">
                    <select readonly="readonly" class="form-control" name="type_id" >
                        @foreach ($types as $type)
                            @if ($accessoire->type_id == $type->id)
                                <option selected value={{$type->id}}>{{$type->nom}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select readonly="readonly" class="form-control" name="couleur_id">
                        @foreach ($couleurs as $couleur)
                            @if ($accessoire->couleur_id == $couleur->id)
                                <option selected value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <a class="btn btn-primary btnFormFROD" href="/accessoires/{{$accessoire->id}}/edit">Modifier</a>
            </div>
    </div>

@endsection
