@extends('layouts.app')

@section('content')
    <div class="table table-hover container-fluid liste card">
        <h1 class="titreFormFROD card-header">Liste d'Accessoires</h1>
        <table>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Nom</th>
                <th scope="col">Prix</th>
                <th scope="col">Type</th>
                <th scope="col">Couleur</th>
            </tr>
            @foreach ($accessoires as $accessoire)
                <tr>
                    <th><a href="/accessoires/{{$accessoire->id}}">{{$accessoire->id}}</a></td>
                    <td>{{$accessoire->nom}}</td>
                    <td>{{$accessoire->prix}}$</td>
                    <td>{{$accessoire->type->nom}}</td>
                    <td>{{$accessoire->couleur->couleur}}</td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/accessoires/create" >Ajouter</a>
    </div>
@endsection
