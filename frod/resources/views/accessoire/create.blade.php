@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Ajout d'un Accessoire</h1>
        <form method="POST" action="/accessoires/">
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <input class="form-control" type="text" min="2" max="12" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" placeholder="Nom" />
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" min="1" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" />
                </div>
                <div class="form-group">
                    <select class="form-control" name="type_id">
                        @foreach ($types as $type)
                            <option value= {{$type->id}}>{{$type->nom}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="couleur_id" >
                        @foreach ($couleurs as $couleur)
                            <option  value= {{$couleur->id}}>{{$couleur->couleur}} </option>
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
            </div>
        </form>
    </div>

@endsection
