@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Modification d'un accessoire</h1>
        <form method="POST" action="/accessoires/{{$accessoire->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <input class="form-control" type="text" min="2" max="12" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value="{{$accessoire->nom}}" />
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" min="1" max="20" step="0.1" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$accessoire->prix}} />
                </div>
                <div class="form-group">
                    <select class="form-control" name="type_id" >
                        @foreach ($types as $type)
                            @if ($accessoire->type_id == $type->id)
                                <option selected value={{$type->id}}>{{$type->nom}}</option>
                            @else
                                <option value={{$type->id}}>{{$type->nom}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="couleur_id">
                        @foreach ($couleurs as $couleur)
                            @if ($accessoire->couleur_id == $couleur->id)
                                <option selected value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @else
                                <option value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @endif
                        @endforeach
                    </select>

                </div>
                <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
            </div>
        </form>
        <button id="btnAccessoireDelete"data-idAccessoire="{{$accessoire->id}}" data-nomaccessoire="{{$accessoire->nom}}" class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
        <form id="formDeleteAccessoire" method="POST" action="/accessoires/{{$accessoire->id}}">
            @method('DELETE')
            @csrf
        </form>
    </div>

@endsection
