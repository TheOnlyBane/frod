@extends('layouts.app')

@section('content')
    <div class="table table-hover container-fluid liste card">
        <h1 class="titreFormFROD">Liste de Couleurs</h1>
        <table>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Couleur</th>
                <th scope="col">Type</th>
                <th scope="col">Prix</th>
                <th scope="col">Html</th>
                <th scope="col">Exemple</th>
            </tr>
            @foreach ($couleurs as $couleur)
                <tr>
                    <th><a href="/couleurs/{{$couleur->id}}">{{$couleur->id}}</a></td>
                    <td>{{$couleur->couleur}}</td>
                    <td>{{$couleur->type}}</td>
                    <td>{{$couleur->prix}}$</td>
                    <td>{{$couleur->html}}</td>
                    <td><span style="font-size:30px; color:{{$couleur->html}}">&#9632;</span></td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/couleurs/create" >Ajouter</a>
    </div>
@endsection
