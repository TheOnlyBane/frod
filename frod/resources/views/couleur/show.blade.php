@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Affichage d'une couleur</h1>
            <div class="content">
                <div class="form-group">
                    <input class="form-control" type="text" readonly="readonly" min="2" max="12" {{$errors->has('couleur') ? 'is-danger' : ''}} name="couleur" value={{$couleur->couleur}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" readonly="readonly" min="1" max="20" step="0.1" {{$errors->has('type') ? 'is-danger' : ''}} name="type" value={{$couleur->type}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" readonly="readonly" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$couleur->prix}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" readonly="readonly" {{$errors->has('html') ? 'is-danger' : ''}} name="html" value={{$couleur->html}} />
                </div>
                <a class="btn btn-primary btnFormFROD" href="/couleurs/{{$couleur->id}}/edit">Modifier</a>
            </div>
    </div>

@endsection
