@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Modification d'une couleur</h1>
        <form method="POST" action="/couleurs/{{$couleur->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <input class="form-control" type="text" min="2" max="12" {{$errors->has('couleur') ? 'is-danger' : ''}} name="couleur" value={{$couleur->couleur}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" min="1" max="20" step="0.1" {{$errors->has('type') ? 'is-danger' : ''}} name="type" value={{$couleur->type}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$couleur->prix}} />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" {{$errors->has('html') ? 'is-danger' : ''}} name="html" value={{$couleur->html}} />
                </div>
                <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
            </div>
        </form>
        <button id="btnCouleurDelete" data-nomCouleur="{{$couleur->couleur}}" data-idCouleur="{{$couleur->id}}" class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
        <form id="formDeleteCouleur" method="POST" action="/couleurs/{{$couleur->id}}">
            @method('DELETE')
            @csrf
        </form>
    </div>

@endsection
