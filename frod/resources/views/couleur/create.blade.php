@extends('layouts.app')
@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Ajout d'une Couleur</h1>
        <form method="POST" action="/couleurs/">
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <input class="form-control" type="text" min="2" max="12" {{$errors->has('couleur') ? 'is-danger' : ''}} name="couleur" placeholder="Couleur" />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" min="1" max="20" step="0.1" {{$errors->has('type') ? 'is-danger' : ''}} name="type" placeholder="Type" />
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
                </div>
                <div class="form-group">
                    <input class="form-control" type="text" {{$errors->has('html') ? 'is-danger' : ''}} name="html" placeholder="Html" />
                </div>
                <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
            </div>
        </form>
    </div>

@endsection
