@extends('layouts.app')

@section('content')
    <div class="liste table-hover container-fluid card">
        <h1 class="titreFormFROD card-header">Liste de Roues</h1>
        <table>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Taille</th>
                <th scope="col">Largeur</th>
                <th scope="col">Prix</th>
                <th scope="col">Pneu</th>
                <th scope="col">Jante</th>
                <th scope="col">Modifier</th>
            </tr>
            @foreach ($roues as $roue)
                <tr>
                    <th><a href="/roues/{{$roue->id}}">{{$roue->id}}</a></td>
                    <td>{{$roue->taille}}</td>
                    <td>{{$roue->largeur}}</td>
                    <td>{{$roue->prix}}$</td>
                    <td>{{$roue->pneu->marquepneu->nom}}</td>
                    <td>{{$roue->jante->marquejante->nom}}</td>
                    <td><a href="/roues/{{$roue->id}}/edit">Modifier</a></td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/roues/create" >Ajouter</a>
    </div>
@endsection
