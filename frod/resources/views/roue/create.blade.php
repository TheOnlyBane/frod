@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD card-header">Ajout d'une roue</h1>
    <form method="POST" action="/roues/">
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input class="form-control" type="number" {{$errors->has('taille') ? 'is-danger' : ''}} name="taille" placeholder="Taille" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" {{$errors->has('largeur') ? 'is-danger' : ''}} name="largeur" placeholder="Largeur" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
            </div>
            <div class="form-group">
                <select class="form-control" name="pneu_id">
                    @foreach ($pneus as $pneu)
                    <option value= {{$pneu->id}}>{{$pneu->marquePneu->nom}} </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                    <select class="form-control" name="jante_id">
                        @foreach ($jantes as $jante)
                        <option value= {{$jante->id}}>{{$jante->marqueJante->nom}} </option>
                        @endforeach
                    </select>
                </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
        </div>
    </form>
</div>  
@endsection