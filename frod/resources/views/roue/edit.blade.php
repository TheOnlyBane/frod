@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD">Modification d'une roue</h1>
    <form method="POST" action="/roues/{{$roue->id}}">
        @method('PATCH')
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input class="form-control" {{$errors->has('taille') ? 'is-danger' : ''}} name="taille" value={{$roue->taille}} />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" {{$errors->has('litre') ? 'is-danger' : ''}} name="largeur" value={{$roue->largeur}} />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$roue->prix}} />
            </div>
            <div class="form-group">
                <select class="form-control" name="pneu_id">
                    @foreach ($pneus as $pneu)
                    @if ($pneu->id == $roue->pneu_id)
                    <option selected value={{$pneu->id}}>{{$pneu->marquePneu->nom}}</option>
                    @else
                    <option value={{$pneu->id}}>{{$pneu->marquePneu->nom}}</option>
                    @endif
                    @endforeach
                </select>

            </div>
            <div class="form-group">
                    <select class="form-control" name="jante_id">
                        @foreach ($jantes as $jante)
                        @if ($jante->id == $roue->jante_id)
                        <option selected value={{$jante->id}}>{{$jante->marqueJante->nom}}</option>
                        @else
                        <option value={{$jante->id}}>{{$jante->marqueJante->nom}}</option>
                        @endif
                        @endforeach
                </select>
                </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
        </div>
    </form>
    <button id="btnRoueDelete" data-idroue={{$roue->id}} data-pneu={{$roue->pneu->marquepneu->nom}} data-jante={{$roue->jante->marquejante->nom}} class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
    <form id="formDeleteRoue" method="POST" action="/roues/{{$roue->id}}">
        @method('DELETE')
        @csrf
    </form>
</div>

@endsection
