@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD">Modification d'une roue</h1>
    <form method="POST" action="/roues/{{$roue->id}}">
        @method('PATCH')
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input readonly class="form-control" {{$errors->has('taille') ? 'is-danger' : ''}} name="taille" value={{$roue->taille}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" {{$errors->has('litre') ? 'is-danger' : ''}} name="largeur" value={{$roue->largeur}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$roue->prix}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" {{$errors->has('pneu') ? 'is-danger' : ''}} name="pneu" value={{$roue->pneu->marquepneu->nom}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" {{$errors->has('jante') ? 'is-danger' : ''}} name="jante" value={{$roue->jante->marquejante->nom}} />
            </div>
            <a class="btn btn-primary" href="/roues">Retour</a>
        </div>
    </form>
    
</div>
    
@endsection