@extends('layouts.app')

@section('content')
<div id="homePage" class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Accueil</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    @if (Auth::check() && Auth::user()->isAdmin == 1)
                        {{-- Tout ce qui concerne l'administrateur --}}
                        <h2>Bienvenue {{Auth::user()->name}}</h2>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Moteurs</h5>
                                <p class="card-text">Tout ce qui concerne les moteurs</p>
                                <a href="/moteurs" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Modeles</h5>
                                <p class="card-text">Tout ce qui concerne les modeles</p>
                                <a href="/modeles" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                        <div class="card-body">
                                <h5 class="card-title">Couleurs</h5>
                                <p class="card-text">Tout ce qui concerne les couleurs</p>
                                <a href="/couleurs" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Pneus</h5>
                                <p class="card-text">Tout ce qui concerne les pneus</p>
                                <a href="/pneus" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Jantes</h5>
                                <p class="card-text">Tout ce qui concerne les jantes</p>
                                <a href="/jantes" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Roues</h5>
                                <p class="card-text">Tout ce qui concerne les roues</p>
                                <a href="/roues" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Accessoires</h5>
                                <p class="card-text">Tout ce qui concerne les accessoires</p>
                                <a href="/accessoires" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">Véhicule</h5>
                                <p class="card-text">Tout ce qui concerne les véhicules</p>
                                <a href="/vehicules" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                        <div class="cardCategorie card ">
                            <div class="card-body">
                                <h5 class="card-title">VueJS</h5>
                                <p class="card-text">Tout ce qui concerne les moteurs</p>
                                <a href="/rouesVueIndex" class="btn btn-primary">Aller</a>
                            </div>
                        </div>
                    @else
                        {{-- Tout ce qui concerne l'utilisateur standard --}}
                        Bienvenue chez FROD. Veuiller passer votre commande !

                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
