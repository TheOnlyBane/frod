@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD">Modification d'un moteur</h1>
    <form method="POST" action="/moteurs/{{$moteur->id}}">
        @method('PATCH')
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input readonly class="form-control" type="number" min="2" max="12" {{$errors->has('cylindre') ? 'is-danger' : ''}} name="cylindre" value={{$moteur->cylindre}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" min="1" max="20" step="0.1" {{$errors->has('litre') ? 'is-danger' : ''}} name="litre" value={{$moteur->litre}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$moteur->prix}} />
            </div>
            <div class="form-group">
                <input readonly class="form-control" type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value={{$moteur->nom}} />
            </div>
            <div class="form-group">
                <textarea readonly class="form-control" {{$errors->has('description') ? 'is-danger' : ''}} name="description"> {{$moteur->description}}</textarea>
            </div>
            <a class="btn btn-primary btnFormFROD" href="/moteurs">Retour</a>
        </div>
    </form>

</div>

@endsection
