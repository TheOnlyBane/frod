@extends('layouts.app')

@section('content')
    <div class="table table-hover container-fluid liste card">
        <h1 class="titreFormFROD card-header">Liste de Moteurs</h1>
        <table>
            <tr>
                <th scope="col">Id</th>
                <th scope="col">Cylindre</th>
                <th scope="col">Litre</th>
                <th scope="col">Prix</th>
                <th scope="col">Nom</th>
                <th scope="col">Description</th>
                <th scope="col">Modifier</th>
            </tr>
            @foreach ($moteurs as $moteur)
                <tr>
                    <td><a href="/moteurs/{{$moteur->id}}">{{$moteur->id}}</a></td>
                    <td>{{$moteur->cylindre}}</td>
                    <td>{{$moteur->litre}}</td>
                    <td>{{$moteur->prix}}$</td>
                    <td>{{$moteur->nom}}</td>
                    <td>{{$moteur->description}}</td>
                    <td><a href="/moteurs/{{$moteur->id}}/edit">Modifier</a></td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/moteurs/create" >Ajouter</a>
    </div>
@endsection
