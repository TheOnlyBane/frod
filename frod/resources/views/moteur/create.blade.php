@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD card-header">Ajout d'un moteur</h1>
    <form method="POST" action="/moteurs/">
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input class="form-control" type="number" min="2" max="12" {{$errors->has('cylindre') ? 'is-danger' : ''}} name="cylindre" placeholder="Cylindre" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="1" max="20" step="0.1" {{$errors->has('litre') ? 'is-danger' : ''}} name="litre" placeholder="Litre" />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
            </div>
            <div class="form-group">
                <input class="form-control" type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" placeholder="Nom" />
            </div>
            <div class="form-group">
                <textarea class="form-control" {{$errors->has('description') ? 'is-danger' : ''}} name="description" placeholder="Description"></textarea>
            </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
        </div>
    </form>
</div>
@endsection
