@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD">Modification d'un moteur</h1>
    <form method="POST" action="/moteurs/{{$moteur->id}}">
        @method('PATCH')
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                    <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <input class="form-control" type="number" min="2" max="12" {{$errors->has('cylindre') ? 'is-danger' : ''}} name="cylindre" value={{$moteur->cylindre}} />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="1" max="20" step="0.1" {{$errors->has('litre') ? 'is-danger' : ''}} name="litre" value={{$moteur->litre}} />
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$moteur->prix}} />
            </div>
            <div class="form-group">
                <input class="form-control" type="text" {{$errors->has('nom') ? 'is-danger' : ''}} name="nom" value={{$moteur->nom}} />
            </div>
            <div class="form-group">
                <textarea class="form-control" {{$errors->has('description') ? 'is-danger' : ''}} name="description"> {{$moteur->description}}</textarea>
            </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
        </div>
    </form>
    <button id="btnMoteurDelete" data-nomMoteur="{{$moteur->nom}}" data-idMoteur="{{$moteur->id}}" class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
    <form id="formDeleteMoteur" method="POST" action="/moteurs/{{$moteur->id}}">
        @method('DELETE')
        @csrf
    </form>
</div>

@endsection
