@extends('layouts.app')

@section('content')

    <h1>Modification d'un véhicule</h1>

    <div class="ModifEnsemblePneu">
        <form method="POST" action="/vehicules/{{$vehicules->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="contentForm">
                <div class="form-group">
                    <select class="form-control" name="modele_id" style="width: 178px">
                        @foreach ($modeles as $modele)
                            @if($vehicules->modele_id==$modele->id)
                                <option selected value= {{$modele->id}}>{{$modele->nom}} </option>
                            @else
                                <option value= {{$modele->id}}>{{$modele->nom}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="moteur_id" style="width: 178px">
                        @foreach ($moteurs as $moteur)
                            @if($vehicules->modele_id==$moteur->id)
                                <option selected value= {{$moteur->id}}>{{$moteur->nom}} </option>
                            @else
                                <option value= {{$moteur->id}}>{{$moteur->nom}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="couleur_id" style="width: 178px">
                        @foreach ($couleurs as $couleur)
                            @if($vehicules->couleur_id==$couleur->id)
                                <option selected value= {{$couleur->id}}>{{$couleur->couleur}} </option>
                            @else
                                <option value= {{$couleur->id}}>{{$couleur->couleur}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="roue_id" style="width: 178px">
                        @foreach ($setroues as $setroue)
                            @if($vehicules->roue_id==$setroue->id)
                                <option selected value= {{$setroue->id}}>{{$setroue->id}} </option>
                            @else
                                <option value= {{$setroue->id}}>{{$setroue->id}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="accessoire_id" style="width: 178px">
                        @foreach ($accessoires as $accesoire)
                            @if($vehicules->accessoire_id==$accesoire->id)
                                <option selected value= {{$accesoire->id}}>{{$accesoire->nom}} </option>
                            @else
                                <option value= {{$accesoire->id}}>{{$accesoire->nom}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="utilisateur_id" style="width: 178px">
                        @foreach ($users as $user)
                            @if($vehicules->utilisateur_id==$user->id)
                                <option selected value= {{$user->id}}>{{$user->name}} </option>
                            @else
                                <option value= {{$user->id}}>{{$user->name}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit">Mise à jour</button>
            </div>
        </form>
    </div>

@endsection
