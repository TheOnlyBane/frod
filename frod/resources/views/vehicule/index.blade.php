@extends('layouts.app')

@section('content')
    <h1>Liste des véhicules</h1>
    <div class="liste">
        <table>
            <tr>
                <th>Id</th>
                <th>Model</th>
                <th>Moteur</th>
                <th>Set de roue</th>
                <th>Couleur</th>
                <th>Accessoire</th>
                <th>Prix</th>
                <th>Acheteur</th>
            </tr>
            @foreach ($vehicules as $vehicule )
                <tr>
                    <th><a href="/vehicules/{{$vehicule->id}}">{{$vehicule->id}}</a></td>
                    <td>
                        Nom: {{$vehicule->modele->nom}} <br>
                        transmission: {{$vehicule->modele->transmission}}
                    </td>
                    <td>
                        Nom: {{$vehicule->moteur->nom}}<br>
                        Nb cilyndre: {{$vehicule->moteur->cylindre}}<br>
                        litre: {{$vehicule->moteur->litre}}<br>
                    </td>
                    <td>
                        Taille: {{$vehicule->roue->taille}} pouces<br>
                        Largeur: {{$vehicule->roue->largeur}} cm<br>
                        Marque Pneu: {{$vehicule->roue->pneu->marquepneu->nom}}<br>
                        type de pneu: {{$vehicule->roue->pneu->type}}<br>
                        Marque Jante: {{$vehicule->roue->jante->marquejante->nom}}<br>
                        Type de Jante: {{$vehicule->roue->jante->type}}<br>
                        Couleur Jante: {{$vehicule->roue->jante->couleur->couleur}}
                    </td>
                    <td>
                        Nom: {{$vehicule->couleur->couleur}}<br>
                        Type: {{$vehicule->couleur->type}}
                    </td>
                    <td>
                        Nom: {{$vehicule->accessoire->nom}}<br>
                        Type: {{$vehicule->accessoire->type->nom}}<br>
                        Couleur{{$vehicule->accessoire->couleur->couleur}}
                    </td>
                    <td>
                        Model véhicule: {{$vehicule->modele->prix}}$<br>
                        Moteur: {{$vehicule->moteur->prix}}$<br>
                        Pneu: {{$vehicule->roue->pneu->prix}}$ <br>
                        Jante: {{$vehicule->roue->jante->prix}}$<br>
                        Accesoire: {{$vehicule->accessoire->prix}}$<br>
                        Couleur jante:{{$vehicule->roue->jante->couleur->prix}}$<br>
                        Couleur véhicule: {{$vehicule->couleur->prix}}$<br>
                        total: {{$vehicule->montantFinal}}$
                    </td>
                    <td>{{$vehicule->utilisateur->name}}</td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/vehicules/create" >Ajouter</a>
    </div>
@endsection
