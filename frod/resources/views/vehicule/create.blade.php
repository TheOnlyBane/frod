@extends('layouts.app')

@section('content')
    <h1 xmlns="http://www.w3.org/1999/html">Ajout d'un véhicule</h1>

    <div class="Cree_vehicule">
        <form method="POST" action="/vehicules/">
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="contentForm">
                <div class="form-group">
                    <select class="form-control" name="modele_id" style="width: 200px">
                        @foreach ($modeles as $modele)
                            <option value= {{$modele->id}}>{{$modele->nom}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="couleur_id" style="width: 200px">
                        @foreach ($moteurs as $moteur)
                            <option value= {{$moteur->id}}>{{$moteur->nom}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="roue_id" style="width: 200px">
                        @foreach ($couleurs as $couleur)
                            <option value= {{$couleur->id}}>{{$couleur->couleur}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="moteur_id" style="width: 200px">
                        @foreach ($setroues as $setroue)
                            <option value= {{$setroue->id}}>{{$setroue->taille}} pouce </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="accessoire_id" style="width: 200px">
                        @foreach ($accessoires as $accesoires)
                            <option value= {{$accesoires->id}}>{{$accesoires->nom}} </option>
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="utilisateur_id" style="width: 200px">
                        @foreach ($users as $user)
                            <option value= {{$user->id}}>{{$user->name}} </option>
                        @endforeach
                    </select>
                </div>
                <button type="submit">Ajouter</button>
            </div>
        </form>
    </div>
@endsection
