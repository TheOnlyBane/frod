@extends('layouts.app')

@section('content')

    <h1>Modification d'un véhicule</h1>
    <form method="get" action="/vehicules/{{$vehicules->id}}/edit">
        <div class="ModifEnsemblePneu">
                <div class="contentForm">
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->modele->nom}}" >
                    </div>
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->moteur->nom}}" >
                    </div>
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->couleur->couleur}}" >
                    </div>
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->roue->id}}" >
                    </div>
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->accessoire->nom}}" >
                    </div>
                    <div class="form-group">
                        <input readonly type="text" value="{{$vehicules->utilisateur->name}}" >
                    </div>
                    <button type="submit">Mise à jour</button>
                </div>
            </div>
        </form>
        <form method="POST" action="/vehicules/{{$vehicules->id}}">
            @method('DELETE')
            @csrf
            <div class="control">
                <button type="submit">Supprimer</button>
            </div>
        </form>
    </div>

@endsection
