@extends('layouts.app')

@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Essemble de jantes</h1>
        <form method="POST" action="/jantes/{{$jante->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <textarea class="form-control" readonly="readonly" {{$errors->has('type') ? 'is-danger' : ''}} name="type">{{$jante->type}}</textarea>
                </div>
                <div class="form-group">
                    <input class="form-control" readonly="readonly" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$jante->prix}} />
                </div>
                <div class="form-group">
                    <select class="form-control" readonly="readonly" name="couleur_id">
                        @foreach ($couleurs as $couleur)
                            @if ($couleur->id == $jante->couleur_id)
                                <option selected value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @else
                                <option value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" readonly="readonly" name="marqueJante_id">
                        @foreach ($marqueJantes as $marqueJante)
                            @if ($marqueJante->id == $jante->marquejante_id)
                                <option selected value={{$marqueJante->id}}>{{$marqueJante->nom}}</option>
                            @else
                                <option value={{$marqueJante->id}}>{{$marqueJante->nom}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <a class="btn btn-primary btnFormFROD" href="/jantes/{{$jante->id}}/edit">Modifier</a>
            </div>
        </form>
    </div>

@endsection
