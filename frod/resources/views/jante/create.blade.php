@extends('layouts.app')

@section('content')


<div class="formFROD card">
    <h1 class="titreFormFROD card-header">Ajout d'un ensemble de jantes</h1>
    <form method="POST" action="/jantes/">
        @csrf
        @if($errors->any())
            <div class="notification is-danger">
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{$error}}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="content">
            <div class="form-group">
                <textarea class="form-control" {{$errors->has('type') ? 'is-danger' : ''}} name="type" placeholder="Type" ></textarea>
            </div>
            <div class="form-group">
                <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
            </div>
            <div class="form-group">
                <select class="form-control" name="couleur_id">
                    @foreach ($couleurs as $couleur)
                        <option value= {{$couleur->id}}>{{$couleur->couleur}} </option>
                    @endforeach
                </select>
            </div>
            <div class="form-group">
                <select class="form-control" name="marqueJante_id">
                    @foreach ($marqueJantes as $marqueJante)
                        <option value= {{$marqueJante->id}}>{{$marqueJante->nom}} </option>
                    @endforeach
                </select>
            </div>
            <button class="btn btn-primary btnFormFROD" type="submit">Ajouter</button>
        </div>
    </form>
</div>
@endsection
