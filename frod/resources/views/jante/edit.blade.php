@extends('layouts.app')

@section('content')

    <div class="formFROD card">
        <h1 class="titreFormFROD">Modification d'un essemble de jantes</h1>
        <form method="POST" action="/jantes/{{$jante->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="content">
                <div class="form-group">
                    <textarea class="form-control" {{$errors->has('type') ? 'is-danger' : ''}} name="type">{{$jante->type}}</textarea>
                </div>
                <div class="form-group">
                    <input class="form-control" type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$jante->prix}} />
                </div>
                <div class="form-group">
                    <select class="form-control" name="couleur_id">
                        @foreach ($couleurs as $couleur)
                            @if ($couleur->id == $jante->couleur_id)
                                <option selected value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @else
                                <option value={{$couleur->id}}>{{$couleur->couleur}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <div class="form-group">
                    <select class="form-control" name="marqueJante_id">
                        @foreach ($marqueJantes as $marqueJante)
                            @if ($marqueJante->id == $jante->marquejante_id)
                                <option selected value={{$marqueJante->id}}>{{$marqueJante->nom}}</option>
                            @else
                                <option value={{$marqueJante->id}}>{{$marqueJante->nom}}</option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button class="btn btn-primary btnFormFROD" type="submit">Mise à jour</button>
            </div>
        </form>
        <button id="btnJanteDelete" data-typeJante="{{$jante->type}}" data-idJante="{{$jante->id}}" class="btn btn-danger btnFormFROD btnDeleteFROD">Supprimer</button>
        <form id="formDeleteJante" method="POST" action="/jantes/{{$jante->id}}">
            @method('DELETE')
            @csrf
        </form>
    </div>

@endsection
