@extends('layouts.app')

@section('content')
    <div class="table table-hover container-fluid liste card">
        <h1 class="titreFormFROD card-header">Liste d'ensembles de jantes</h1>
            <table>
                <tr>
                    <th scope="col">Id</th>
                    <th scope="col">Type</th>
                    <th scope="col">Prix</th>
                    <th scope="col">Couleur</th>
                    <th scope="col">Marque de la Jante</th>
                </tr>
                @foreach ($jantes as $jante)
                    <tr>
                        <th><a href="/jantes/{{$jante->id}}">{{$jante->id}}</a></td>
                        <td>{{$jante->type}}</td>
                        <td>{{$jante->prix}}$</td>
                        <td>{{$jante->couleur->couleur}}</td>
                        <td>{{$jante->marqueJante->nom}}</td>
                    </tr>
                @endforeach
            </table>
        <a class="btn btn-primary" href="/jantes/create" >Ajouter</a>
    </div>
@endsection
