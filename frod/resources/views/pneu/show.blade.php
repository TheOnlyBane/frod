@extends('layouts.app')

@section('content')
    <h1>Modification d'un enssemble de pneu</h1>

    <div class="ModifEnsemblePneu">
        <form method="get" action="/pneus/{{$pneus->id}}/edit">

            <div class="contentForm">
                <div class="control">
                    <input readonly type="textarea" value="{{$pneus->type}}"/>
                </div>
                <div class="control">
                    <input readonly type="text"  value={{$pneus->prix}}$ />
                </div>
                <div class="control">
                    <input readonly type="text"  value={{$pneus->saison}} />
                </div>
                <div class="form-group">
                    <input readonly type="text"  value={{$pneus->marquepneu->nom}} />
                </div>
                <button type="submit">Mise à jour</button>
            </div>
        </form>

    </div>

@endsection
