@extends('layouts.app')

@section('content')
    <h1>Modification d'un enssemble de pneu</h1>

    <div class="ModifEnsemblePneu">
        <form method="POST" action="/pneus/{{$pneu->id}}">
            @method('PATCH')
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="contentForm">
                <div class="control">
                    <input type="textarea" {{$errors->has('type') ? 'is-danger' : ''}} name="type" value="{{$pneu->type}}" />
                </div>
                <div class="control">
                    <input type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" value={{$pneu->prix}} />
                </div>
                <div class="control">
                    <input type="text" {{$errors->has('saison') ? 'is-danger' : ''}} name="saison" value={{$pneu->saison}} />
                </div>
                <div class="form-group">
                    <select class="form-control" name="marquepneu_id" style="width: 178px">
                        @foreach ($marquepneus as $marquepneu)
                            @if($pneu->marquepneu_id==$marquepneu->id)
                                <option selected value= {{$marquepneu->id}}>{{$marquepneu->nom}} </option>
                            @else
                                <option value= {{$marquepneu->id}}>{{$marquepneu->nom}} </option>
                            @endif
                        @endforeach
                    </select>
                </div>
                <button type="submit">Mise à jour</button>
            </div>
        </form>
        <form method="POST" action="/pneus/{{$pneu->id}}">
            @method('DELETE')
            @csrf
            <div class="control">
                <button type="submit">Supprimer</button>
            </div>
        </form>
    </div>

@endsection
