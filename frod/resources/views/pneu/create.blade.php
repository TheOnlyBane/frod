@extends('layouts.app')

@section('content')

    <h1>Ajout d'un enssemble de pneu</h1>

    <div class="ModifEnsemblePneu">
        <form method="POST" action="/pneus/">
            @csrf
            @if($errors->any())
                <div class="notification is-danger">
                    <ul>
                        @foreach ($errors->all() as $error)
                            <li>{{$error}}</li>
                        @endforeach
                    </ul>
                </div>
            @endif
            <div class="contentForm">
                <div class="control">
                    <input type="textarea"  {{$errors->has('type') ? 'is-danger' : ''}} name="type" placeholder="Type" />
                </div>
                <div class="control">
                    <input type="number" min="0" step="0.01" {{$errors->has('prix') ? 'is-danger' : ''}} name="prix" placeholder="Prix" />
                </div>
                <div class="control">
                    <input type="text"  {{$errors->has('saison') ? 'is-danger' : ''}} name="saison" placeholder="Saison" />
                </div>
                <div class="form-group">
                    <select class="form-control" name="marquepneu_id" style="width: 178px">
                        @foreach ($marquepneus as $marquepneu)
                            <option value= {{$marquepneu->id}}>{{$marquepneu->nom}} </option>
                        @endforeach
                    </select>
                </div>
                <button type="submit">Ajouter</button>
            </div>
        </form>
    </div>
@endsection
