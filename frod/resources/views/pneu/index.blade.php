@extends('layouts.app')

@section('content')
    <h1>Liste des enssemble de pneu</h1>
    <div class="liste">
        <table>
            <tr>
                <th>Id</th>
                <th>Type</th>
                <th>Prix</th>
                <th>Saison</th>
                <th>Marque pneu</th>
            </tr>
            @foreach ($pneus as $pneu )
                <tr>
                    <th><a href="/pneus/{{$pneu->id}}">{{$pneu->id}}</a></th>
                    <td>{{$pneu->type}}</td>
                    <td>{{$pneu->prix}}$</td>
                    <td>{{$pneu->saison}}</td>
                    <td>{{$pneu->marquepneu->nom}}</td>
                </tr>
            @endforeach
        </table>
        <a class="btn btn-primary" href="/pneus/create" >Ajouter</a>
    </div>
@endsection
