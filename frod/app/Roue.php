<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Roue extends Model
{

    public $timestamps = false;
    protected $fillable = [
        'taille',
        'largeur',
        'prix',
        'pneu_id',
        'jante_id'
    ];
    public function pneu(){
        return $this->belongsTo('App\Pneu');
    }
    public function jante(){
        return $this->belongsTo('App\Jante');
    }
    public function vehicule() {
        return $this->hasMany('App\Vehicule');
    }
}
