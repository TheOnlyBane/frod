<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Marquepneu extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'nom'
    ];

    public function pneu(){
        return $this->hasMany('App\Pneu');
    }
}
