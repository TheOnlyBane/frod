<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class jante extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'type',
        'prix',
        'couleur_id',
        'marqueJante_id'
    ];

    public function marquejante(){
        return $this->belongsTo('App\Marquejante');
    }
    public function couleur(){
        return $this->belongsTo('App\Couleur');
    }
    public function roue(){
        return $this->hasMany('App\Roue');
    }

}
