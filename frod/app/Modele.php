<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Modele extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'nom',
        'prix',
        'description',
        'transmission',
        'annee'
    ];
    public function vehicule(){
        return $this->hasMany('App\Vehicule');
    }
}
