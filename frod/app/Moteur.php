<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Moteur extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'cylindre',
        'litre',
        'prix',
        'nom',
        'description'
    ];

}
