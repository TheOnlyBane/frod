<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Pneu extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'type',
        'prix',
        'saison',
        'marquepneu_id'
    ];

    public function marquepneu(){
        return $this->belongsTo('App\Marquepneu');
    }
    public function roue(){
        return $this->hasMany('App\Roue');
    }
}
