<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Couleur extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'couleur',
        'type',
        'prix',
        'html'
    ];
    public function vehicule()
    {
        return $this->hasMany('App\Vehicule');
    }
    public function jante(){
        return $this->hasMany('App\Jante');

    }
}
