<?php


namespace App;
use Illuminate\Database\Eloquent\Model;

class Vehicule extends model
{
    public $timestamps = false;
    protected $fillable = [
        'montantFinal',
        'moteur_id',
        'modele_id',
        'couleur_id',
        'roue_id',
        'accessoire_id',
        'utilisateur_id'
    ];

    public function moteur(){
        return $this->belongsTo('App\Moteur');
    }
    public function modele(){
        return $this->belongsTo('App\Modele');
    }
    public function utilisateur(){
        return $this->belongsTo('App\User');
    }
    public function couleur(){
        return $this->belongsTo('App\Couleur');
    }
    public function roue(){
        return $this->belongsTo('App\Roue');
    }
    public function accessoire(){
        return $this->belongsTo('App\Accessoire');
    }
}
