<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Accessoire extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'nom',
        'prix',
        'couleur_id',
        'type_id'
    ];
    public function couleur(){
        return $this->belongsTo('App\Couleur');
    }
    public function type(){
        return $this->belongsTo('App\Type');
    }
    public function vehicule() {
        return $this->hasMany('App\Vehicule');
    }
}
