<?php

namespace App\Http\Middleware;

use Closure;
class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    protected $auth;
    // public function __construct(Guard $auth)
    // {
    //     $this->auth = $auth;
    // }
    public function handle($request, Closure $next)
    {
        if ($request->user() && $request->user()->isAdmin != 1)
        {
            abort(403, 'Action non autorisée!');
        }
        return $next($request);
    }
}
