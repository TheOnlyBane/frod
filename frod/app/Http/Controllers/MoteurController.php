<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Moteur;

class MoteurController extends Controller
{
    /**
     * Affiche une liste de ressources.
     *
     * @return \Illuminate\Http\Response La vue de la liste des moteurs
     */
    public function index()
    {
        $moteurs = Moteur::all();
        return view('moteur.index', compact('moteurs'));
    }

    /**
     * Montrer le formulaire pour créer une nouvelle ressource.
     *
     * @return \Illuminate\Http\Response La vue du formulaire de création
     */
    public function create()
    {
        return view('moteur.create');
    }

    /**
     * Stocker une ressource nouvellement créée dans le stockage.
     *
     * @param  \Illuminate\Http\Request  $request La requête HTTP
     * @return \Illuminate\Http\Response La vue de la liste des moteurs
     */
    public function store(Request $request)
    {
        $request->validate([
            'cylindre'=>['required'],
            'litre'=>['required'],
            'prix'=>['required']
        ]);
        $moteur = new Moteur();
        $moteur->cylindre = $request->get('cylindre');
        $moteur->litre = $request->get('litre');
        $moteur->prix = $request->get('prix');
        $moteur->nom = $request->get('nom');
        $moteur->description = $request->get('description');
        $moteur->save();
        return redirect('/moteurs');
    }

    /**
     * Afficher la ressource spécifiée.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue du 'show' d'un moteur
     */
    public function show($id)
    {
        $moteur = Moteur::findorfail($id);
        return view('moteur.show', compact('moteur'));
    }

    /**
     * Afficher le formulaire pour éditer la ressource spécifiée.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue du formulaire de modification
     */
    public function edit($id)
    {
        $moteur = Moteur::find($id);
        return view('moteur.edit', compact('moteur'));
    }

    /**
     * Mettre à jour la ressource spécifiée en stockage.
     *
     * @param  \Illuminate\Http\Request  $request REquête HTTP
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue de la liste des moteurs
     */
    public function update(Request $request, $id)
    {
        $moteur = Moteur::find($id);
        $request->validate([
            'cylindre'=>['required'],
            'litre'=>['required'],
            'prix'=>['required']
        ]);
        $moteur->cylindre = $request->get('cylindre');
        $moteur->litre = $request->get('litre');
        $moteur->prix = $request->get('prix');
        $moteur->nom = $request->get('nom');
        $moteur->description = $request->get('description');
        $moteur->save();
        
        return redirect("/moteurs");

    }

    /**
     * Supprimer la ressource spécifiée du stockage.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue de la liste des moteurs
     */
    public function destroy($id)
    {
        $moteur = Moteur::find($id);
        $moteur->delete();
        return redirect("/moteurs");
    }
}
