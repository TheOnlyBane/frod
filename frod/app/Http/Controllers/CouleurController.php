<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Couleur;

class CouleurController extends Controller
{
    public function index()
    {
        $couleurs = Couleur::all();
        return view('couleur.index', compact('couleurs'));
    }

    public function create()
    {
        return view('couleur.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'couleur'=>['required'],
            'type'=>['required'],
            'prix'=>['required'],
            'html'=>['required']
        ]);
        Couleur::create([
            'couleur'=>$request->get('couleur'),
            'type'=>$request->get('type'),
            'prix'=>$request->get('prix'),
            'html'=>$request->get('html')
        ]);
        return redirect('/couleurs');
    }

    public function show($id)
    {
        $couleur = Couleur::find($id);
        return view('couleur.show', compact('couleur'));
    }

    public function edit($id)
    {
        $couleur = Couleur::find($id);
        return view('couleur.edit', compact('couleur'));
    }

    public function update(Request $request, $id)
    {
        $couleur = Couleur::find($id);
        $request->validate([
            'couleur'=>['required'],
            'type'=>['required'],
            'prix'=>['required'],
            'html'=>['required']
        ]);
        $couleur->couleur = $request->get('couleur');
        $couleur->type = $request->get('type');
        $couleur->prix = $request->get('prix');
        $couleur->html = $request->get('html');
        $couleur->save();
        return redirect ("/couleurs");
    }

    public function destroy($id)
    {
        $couleur = Couleur::find($id);
        $couleur->delete();
        return redirect('/couleurs');
    }
}
