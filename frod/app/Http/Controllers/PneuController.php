<?php


namespace App\Http\Controllers;


use App\MarquePneu;
use App\Pneu;
use App\Http\Resources\PneuResource;
use Illuminate\Http\Request;

class PneuController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $pneus = Pneu::all();

        return view('pneu.index', compact('pneus'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $marquepneus=MarquePneu::all();
        $pneus = Pneu::all();
        return view('pneu.create',compact('marquepneus','pneus'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $request->validate([
            'type'=>['required'],
            'prix'=>['required'],
            'saison'=>['required'],
            'marquepneu_id'=>['required']
        ]);
        Pneu::create([
            'type'=>$request->get('type'),
            'prix'=>$request->get('prix'),
            'saison'=>$request->get('saison'),
            'marquepneu_id'=>$request->get('marquepneu_id')
        ]);
        return redirect('/pneus');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $pneus=Pneu::find($id);
        return view('pneu.show',compact('pneus'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $marquepneus=MarquePneu::all();
        $pneu = Pneu::find($id);
        return view('pneu.edit', compact('pneu','marquepneus'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $pneu = Pneu::find($id);
        $request->validate([
            'type'=>['required'],
            'prix'=>['required'],
            'saison'=>['required'],
            'marquepneu_id'=>['required']
        ]);
        $pneu->type = $request->get('type');
        $pneu->prix = $request->get('prix');
        $pneu->saison = $request->get('saison');
        $pneu->marquePneu_id = $request->get('marquepneu_id');
        $pneu->save();

        return redirect('/pneus');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $pneu = Pneu::find($id);
        $pneu->delete();
        return redirect('/pneus');
    }

    //! VueJS

    public function listeApi(){
        PneuResource::withoutWrapping();
        $pneus = Pneu::all();
        return PneuResource::collection($pneus);
    }
}
