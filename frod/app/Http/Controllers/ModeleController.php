<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Modele;

class ModeleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $modeles = Modele::all();
        return view('modele.index', compact('modeles'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('modele.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nom'=>['required'],
            'prix'=>['required'],
            'description'=>['required'],
            'transmission'=>['required'],
            'annee'=>['required']
        ]);
        modele::create([
            'nom'=>$request->get('nom'),
            'prix'=>$request->get('prix'),
            'description'=>$request->get('description'),
            'transmission'=>$request->get('transmission'),
            'annee'=>$request->get('annee')
        ]);
        return redirect('/modeles');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $modele = Modele::find($id);
        return view('modele.show', compact('modele'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $modele = Modele::find($id);
        return view('modele.edit', compact('modele'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $modele = Modele::find($id);
        $request->validate([
            'nom'=>['required'],
            'prix'=>['required'],
            'description'=>['required'],
            'transmission'=>['required'],
            'annee'=>['required']
        ]);
        $modele->nom = $request->get('nom');
        $modele->prix = $request->get('prix');
        $modele->description = $request->get('description');
        $modele->transmission = $request->get('transmission');
        $modele->annee = $request->get('annee');
        $modele->save();

        return redirect("/modeles");

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $modele = Modele::find($id);
        $modele->delete();
        return redirect('/modeles');
    }
}
