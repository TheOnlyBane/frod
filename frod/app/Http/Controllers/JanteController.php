<?php


namespace App\Http\Controllers;

use App\Couleur;
use App\Marquejante;
use App\Jante;
use App\Http\Resources\JanteResource;
use Illuminate\Http\Request;

class JanteController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $jantes = Jante::all();
        return view('jante.index',  compact('jantes') );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $couleurs = Couleur::all();
        $marqueJantes = Marquejante::all();
        return view('jante.create',  compact('couleurs', 'marqueJantes') );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'type'=>['required'],
            'prix'=>['required'],
            'couleur_id'=>['required'],
            'marqueJante_id'=>['required']
        ]);
        Jante::create([
            'type'=>$request->get('type'),
            'prix'=>$request->get('prix'),
            'couleur_id'=>$request->get('couleur_id'),
            'marqueJante_id'=>$request->get('marqueJante_id')
        ]);
        return redirect('/jantes');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $couleurs = Couleur::all();
        $marqueJantes = Marquejante::all();
        $jante = Jante::find($id);
        return view('jante.show', compact('jante', 'couleurs', 'marqueJantes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $couleurs = Couleur::all();
        $marqueJantes = Marquejante::all();
        $jante = Jante::find($id);
        return view('jante.edit', compact('jante', 'couleurs', 'marqueJantes'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $jante = Jante::find($id);
        $request->validate([
            'type'=>['required'],
            'prix'=>['required'],
            'couleur_id'=>['required'],
            'marqueJante_id'=>['required']
        ]);
        $jante->type = $request->get('type');
        $jante->prix = $request->get('prix');
        $jante->couleur_id = $request->get('couleur_id');
        $jante->marqueJante_id = $request->get('marqueJante_id');
        $jante->save();

        return redirect('/jantes');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $jante = Jante::find($id);
        $jante->delete();
        return redirect('/jantes');
    }

    // !VueJS
    public function listeApi(){
        JanteResource::withoutWrapping();
        $jantes = Jante::all();
        return JanteResource::collection($jantes);
    }
}
