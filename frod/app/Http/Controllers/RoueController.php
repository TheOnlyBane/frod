<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\StoreRoue;

use App\Roue;
use App\Pneu;
use App\Jante;
use App\Http\Resources\RoueResource;
class RoueController extends Controller
{
    /**
     * Affiche une liste de la ressource.
     *
     * @return \Illuminate\Http\Response La vue de la liste des roues
     */
    public function index()
    {
        $roues = Roue::all();
        return view('roue.index', compact('roues'));
    }

    /**
     * Afficher le formulaire pour créer une nouvelle ressource.
     *
     * @return \Illuminate\Http\Response  La vue du formulaire de création
     */
    public function create()
    {
        $pneus = Pneu::all();
        $jantes = Jante::all();
        return view('roue.create', compact('pneus', 'jantes'));
    }

    /**
     * Stocker une ressource nouvellement créée dans le stockage.
     *
     * @param  \Illuminate\Http\Request  $request Requête HTTP
     * @return \Illuminate\Http\Response La vue de la liste des roues
     */
    public function store(Request $request)
    {
        $request->validate([
            'taille' => ['required'],
            'largeur' => ['required'],
            'prix' => ['required']
        ]);
        $roue = new Roue();
        $roue->taille = $request->get('taille');
        $roue->largeur = $request->get('largeur');
        $roue->prix = $request->get('prix');
        $roue->pneu()->associate($request->get('pneu_id'));
        $roue->jante()->associate($request->get('jante_id'));
        $roue->save();
        return redirect('/roues');
    }

    /**
     * Afficher la ressource spécifiée.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue du 'show' de la ressource
     */
    public function show($id)
    {
        $roue = Roue::findorfail($id);
        return view('roue.show', compact('roue'));
    }

    /**
     * Afficher le formulaire pour éditer la ressource spécifiée.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue du formulaire de modification
     */
    public function edit(Roue $roue)
    {
        $pneus = Pneu::all();
        $jantes = Jante::all();
        return view('roue.edit', compact('roue', 'pneus', 'jantes'));
    }

    /**
     * Mettre à jour la ressource spécifiée en stockage.
     *
     * @param  \Illuminate\Http\Request  $request  Requête HTTP
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response Le vue de la liste de roues
     */
    public function update(Request $request, Roue $roue)
    {
        $roue->taille = $request->get('taille');
        $roue->largeur = $request->get('largeur');
        $roue->prix = $request->get('prix');
        $roue->pneu_id = $request->get('pneu_id');
        $roue->jante_id = $request->get('jante_id');
        $roue->save();

        return redirect('/roues');
    }   

    /**
     * Supprimer la ressource spécifiée du stockage.
     *
     * @param  int  $id Le id de la ressource
     * @return \Illuminate\Http\Response La vue de la liste de roues
     */
    public function destroy(Roue $roue)
    {
        $roue->delete();
        return redirect('/roues');
    }

    // ! Section Vue

  
    /**
     * Affiche la vue avec le template
     *
     * @return Response La vue avec le router de Vue
     */
    public function indexVue(){
        return view('roue.vue');
    }

    /**
     * Liste les ressources de type Roue
     *
     * @return  RoueRessource  La collection de ressource demandée
     */
    public function listeApi(){
        RoueResource::withoutWrapping();
        return RoueResource::collection(Roue::all());
    }
    /**
     * Affiche le formulaire de modification de la ressource
     *
     * @param   Request  $request  La requête HTTP
     * @param   int   $id          L'id de la ressource
     *
     * @return  Reponse            La reponse en JSON
     */
    public function editApi(Request $request, $id) {
        RoueResource::withoutWrapping();
        return new RoueResource(Roue::find($id));
    }

    /**
     * Supprime la ressource de la base de données
     *
     * @param   Request  $request  La requête HTTP
     * @param   int      $id       L'id de la ressource à détruire
     *
     * @return  Response           La réponse en JSON
     */
    public function deleteApi(Request $request, $id) {
        Roue::find($id)->delete();
        return response()->json();
    }

    /**
     * Ajoute la nouvelle ressource dans la base de données
     *
     * @param   StoreRoue  $request  La requête HTTP
     *
     * @return  Response             La réponse
     */
    public function storeApi(StoreRoue $request){
        $roue = new Roue();
        $roue->taille = $request['taille'];
        $roue->largeur = $request['largeur'];
        $roue->prix = $request['prix'];
        $roue->pneu()->associate($request['pneu_id']);
        $roue->jante()->associate($request['jante_id']);
        $roue->save();
        return response($roue->id, 200);
    }
    
    /**
     * Modifie la ressource dans la base de données
     *
     * @param   StoreRoue  $request  La requete HTTP
     * @param   int $id Le id de la ressource à modifier
     *
     * @return  Response  La réponse
     */
    public function updateApi(StoreRoue $request, $id) {
        $roue = Roue::findorfail($id);
        $roue->taille = $request['taille'];
        $roue->largeur = $request['largeur'];
        $roue->prix = $request['prix'];
        $roue->pneu()->associate($request['pneu_id']);
        $roue->jante()->associate($request['jante_id']);
        $roue->save();
        return response($roue->id, 200);
    }
    /**
     * Va chercher la liste de ressource dans la base de données
     *
     * @param   Request  $request  La requête HTTP
     * @param   int   $id          Le id de la ressource
     *
     * @return  RoueRessource      La ressource demandée
     */
    public function showApi(Request $request, $id){
        RoueResource::withoutWrapping();
        return new RoueResource(Roue::find($id));
    }
}

?>
