<?php

namespace App\Http\Controllers;

use App\Couleur;
use App\Type;
use Illuminate\Http\Request;
use App\Accessoire;

class AccessoireController extends Controller
{
    public function index()
    {
        $accessoires=Accessoire::all();
        return view('accessoire.index', compact('accessoires'));
    }

    public function create()
    {
        $couleurs = Couleur::all();
        $types = Type::all();
        return view('accessoire.create',compact('couleurs', 'types'));
    }

    public function store(Request $request)
    {
        $request->validate([
            'nom'=>['required'],
            'prix'=>['required']
        ]);
        $accessoire = Accessoire::create([
            'nom'=>$request->get('nom'),
            'prix'=>$request->get('prix'),
            'type_id'=>$request->get('type_id'),
            'couleur_id'=>$request->get('couleur_id')
        ]);
        $accessoire->save();
        return redirect('/accessoires');
    }

    public function show(Accessoire $accessoire)
    {
        $couleurs = Couleur::all();
        $types = Type::all();
        return view('accessoire.show',compact('accessoire','couleurs', 'types'));
    }

    public function edit(Accessoire $accessoire)
    {
        $couleurs = Couleur::all();
        $types = Type::all();
        return view('accessoire.edit',compact('accessoire','couleurs', 'types'));
    }

    public function update(Request $request, Accessoire $accessoire)
    {
        $accessoire->nom = $request->get('nom');
        $accessoire->prix = $request->get('prix');
        $accessoire->type_id = $request->get('type_id');
        $accessoire->couleur_id = $request->get('couleur_id');
        $accessoire->save();
        return redirect ("/accessoires");
    }

    public function destroy($id)
    {
        $accessoire = Accessoire::find($id);
        $accessoire->delete();
        return redirect('/accessoires');
    }
}
