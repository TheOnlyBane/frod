<?php


namespace App\Http\Controllers;

use App\Accessoire;
use App\Modele;
use App\Moteur;
use App\Couleur;
use App\Roue;
use App\User;
use App\Vehicule;
use Illuminate\Http\Request;

class VehiculeController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $vehicules = Vehicule::all();

        return view('vehicule.index', compact('vehicules'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $vehicules = Vehicule::all();
        $moteurs = Moteur::all();
        $modeles = Modele::all();
        $couleurs= Couleur::all();
        $setroues=Roue::all();
        $accessoires=Accessoire::all();
        $users=User::all();
        return view('vehicule.create',compact('vehicules','moteurs','modeles','couleurs','setroues'
            ,'users','accessoires'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {

        $request->validate([
            'moteur_id'=>['required'],
            'modele_id'=>['required'],
            'utilisateur_id'=>['required'],
            'couleur_id'=>['required'],
            'roue_id'=>['required'],
            'accessoire_id'=>['required']

        ]);
        $new = Vehicule::create([
            'moteur_id'=>$request->get('moteur_id'),
            'modele_id'=>$request->get('modele_id'),
            'utilisateur_id'=>$request->get('utilisateur_id'),
            'couleur_id'=>$request->get('couleur_id'),
            'roue_id'=>$request->get('roue_id'),
            'accessoire_id'=>$request->get('accessoire_id'),
            'montantFinal'=>0
        ]);
        $moteurprix = Moteur::find($new->moteur_id);
        $modelprix = Modele::find($new->modele_id);
        $couleurprix = Couleur::find($new->couleur_id);
        $roueprix = Roue::find($new->roue_id);
        $accessoireprix = Accessoire::find($new->accessoire_id);
        $new->montantFinal=$moteurprix->prix + $modelprix->prix + $couleurprix->prix + $roueprix->prix+
            $accessoireprix->prix;
        $new->save();
        return redirect('/vehicules');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $vehicules = Vehicule::find($id);
        return view('vehicule.show',compact('vehicules'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $vehicules = Vehicule::find($id);
        $moteurs = Moteur::all();
        $modeles = Modele::all();
        $couleurs= Couleur::all();
        $setroues=Roue::all();
        $accessoires=Accessoire::all();
        $users=User::all();
        return view('vehicule.edit',compact('vehicules','moteurs','modeles','couleurs','setroues'
            ,'users','accessoires'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $vehicule = Vehicule::find($id);
        $request->validate([
            'moteur_id'=>['required'],
            'modele_id'=>['required'],
            'utilisateur_id'=>['required'],
            'couleur_id'=>['required'],
            'roue_id'=>['required'],
            'accessoire_id'=>['required']
        ]);
        $vehicule->moteur_id = $request->get('moteur_id');
        $vehicule->modele_id = $request->get('modele_id');
        $vehicule->utilisateur_id = $request->get('utilisateur_id');
        $vehicule->couleur_id = $request->get('couleur_id');
        $vehicule->roue_id = $request->get('roue_id');
        $vehicule->accessoire_id = $request->get('accessoire_id');
        $vehicule->montantFinal=$vehicule->moteur->prix+ $vehicule->modele->prix+ $vehicule->couleur->prix +
            $vehicule->roue->prix+ $vehicule->accessoire->prix;
        $vehicule->save();

        return redirect('/vehicules');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $vehicule = Vehicule::find($id);
        $vehicule->delete();
        return redirect('/vehicules');
    }
}
