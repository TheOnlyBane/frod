<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Pneu;
use App\MarquePneu;
class PneuResource extends JsonResource
{
    /**
     * Transformez la ressource en tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $marquePneu =  new MarquePneuResource(MarquePneu::findorfail($this->marquepneu_id));
        return [
            'id' => (string)$this->id,
            'type' => $this->type,
            'prix' => $this->prix,
            'saison' => $this->saison,
            'marquepneu' => $marquePneu,
            'pneu' => $marquePneu->nom
        ];
    }
}
