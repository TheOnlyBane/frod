<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Pneu;
use App\Jante;

class RoueResource extends JsonResource
{
    /**
     * Transformez la ressource en tableau.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $pneu = new PneuResource(Pneu::findorfail($this->pneu_id));
        $jante = new JanteResource(Jante::findorfail($this->jante_id));
        return [
            'id' => (string)$this->id,
            'taille' => $this->taille,
            'largeur' => $this->largeur,
            'prix' => $this->prix,
            'pneu' => $pneu->marquepneu->nom,
            'pneu_id' => $this->pneu()->pluck('id'),
            'jante' => $jante->marquejante->nom,
            'jante_id' => $this->jante()->pluck('id')
        ];
    }
}
