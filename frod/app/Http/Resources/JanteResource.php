<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;
use App\Jante;
use App\Couleur;
use App\Marquejante;
class JanteResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        $marquejante = new MarqueJanteResource(Marquejante::findorfail($this->marquejante_id));
        return [
            'id' => (string) $this->id,
            'type'=> $this->type,
            'prix'=> $this->prix,
            'couleur'=> new CouleurResource(Couleur::findorfail($this->couleur_id)),
            'marquejante'=> $marquejante,
            'jante' => $marquejante->nom
        ];
    }
}
