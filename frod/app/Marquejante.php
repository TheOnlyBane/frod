<?php


namespace App;

use Illuminate\Database\Eloquent\Model;

class Marquejante extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'id',
        'nom'
    ];
    public function jante(){
        return $this->hasMany('App\Jante');
    }
}
