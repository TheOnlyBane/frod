jQuery(document).ready(function(){
    jQuery.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': jQuery('meta[name="csrf-token"]').attr('content')
        }
    });
    jQuery('#btnMoteurDelete').on("click", function(){
        var nomMoteur = jQuery(this).data("nommoteur");
        var idMoteur = jQuery(this).data("idmoteur");
        Swal.fire({
            title: 'Suppression de moteur',
            text: "Êtes-vous sur de supprimer : "+nomMoteur+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
          }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteMoteur").submit();
            }
        });
    });
    jQuery('#btnModeleDelete').on("click", function(){
        var nomModele = jQuery(this).data("nommodele");
        var idModele = jQuery(this).data("idmodele");
        Swal.fire({
            title: 'Suppression de modele',
            text: "Êtes-vous sur de supprimer : "+nomModele+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteModele").submit();
            }
        });
    });
    jQuery('#btnCouleurDelete').on("click", function(){
        var nomCouleur = jQuery(this).data("nomcouleur");
        var idCouleur = jQuery(this).data("idcouleur");
        Swal.fire({
            title: 'Suppression de couleur',
            text: "Êtes-vous sur de supprimer : "+nomCouleur+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteCouleur").submit();
            }
        });
    });
    jQuery('#btnAccessoireDelete').on("click", function(){
        var nomAccessoire = jQuery(this).data("nomaccessoire");
        var idAccessoire = jQuery(this).data("idaccessoire");
        Swal.fire({
            title: "Suppression d'un accessoire",
            text: "Êtes-vous sur de supprimer : "+nomAccessoire+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteAccessoire").submit();
            }
        });
    });
    jQuery('#btnJanteDelete').on("click", function(){
        var typeJante = jQuery(this).data("typejante");
        var idJante = jQuery(this).data("idjante");
        Swal.fire({
            title: 'Suppression de jantes',
            text: "Êtes-vous sur de supprimer : "+typeJante+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
        }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteJante").submit();
            }
        });
    });
    jQuery('#btnRoueDelete').on("click", function(){
        var pneu = jQuery(this).data("pneu");
        var jante = jQuery(this).data("jante");
        var idRoue = jQuery(this).data("idroue");
        Swal.fire({
            title: 'Suppression de moteur',
            text: "Êtes-vous sur de supprimer : "+pneu+" " + jante+" ?",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Oui'
          }).then((result) => {
            if (result.value) {
                jQuery("#formDeleteRoue").submit();
            }
        });
    });
})
