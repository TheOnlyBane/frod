<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['middleware' =>['admin']], function(){

    // Routes pour les roues
    Route::get('roues/listeApi', ['as' => 'roues.liste', 'uses' => 'RoueController@listeApi']);
    Route::post('roues/storeApi/', ['as' => 'roues.storeApi', 'uses' => 'RoueController@storeApi']);
    Route::get('roues/editApi/{id}', ['as' => 'roues.editApi', 'uses' => 'RoueController@editApi']);
    Route::get('roues/showApi/{id}', ['as' => 'roues.showApi', 'uses' => 'RoueController@showApi']);
    Route::get('roues/deleteApi/{id}', ['as' => 'roues.deleteApi', 'uses' => 'RoueController@deleteApi']);
    Route::post('roues/updateApi/{id}', ['as' => 'roues.updateApi', 'uses' => 'RoueController@updateApi']);
    Route::get('pneus/listePneusPourSelecteur', ['as' => 'pneus.liste', 'uses' => 'PneuController@listeApi']);
    Route::get('jantes/listeJantesPourSelecteur', ['as' => 'jantes.liste', 'uses' => 'JanteController@listeApi']);
});