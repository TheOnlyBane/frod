<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('home');
});
Route::get('/rouesVueIndex', function () {
    return view('roue.vue');
});

Auth::routes();
Route::get('/home', 'HomeController@index')->name('home');
// ! Routes accessibles par l'administrateur seulement
Route::middleware(['admin'])->group(function(){
    Route::resource('moteurs', 'MoteurController');
    Route::resource('modeles', 'ModeleController');
    Route::resource('pneus', 'PneuController');
    Route::resource('couleurs', 'CouleurController');
    Route::resource('accessoires', 'AccessoireController');
    Route::resource('roues', 'RoueController');
    Route::resource('pneus', 'PneuController');
    Route::resource('jantes', 'JanteController');
    Route::resource('pneus', 'PneuController');
    Route::resource('vehicules', 'VehiculeController');
    Route::get('rouesVueIndex', 'RoueController@indexVue');
})

?>
