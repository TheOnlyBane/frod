<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehiculeRoue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Vehicule_Roues', function(Blueprint $table){
            $table->bigInteger('vehicule_id')->unsigned();
            $table->bigInteger('roue_id')->unsigned();
            $table->foreign('vehicule_id')->references('id')->on('Vehicules');
            $table->foreign('roue_id')->references('id')->on('Roues');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Vehicule_Roues');
    }
}
