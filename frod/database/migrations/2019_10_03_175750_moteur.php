<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Moteur extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Moteurs', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('cylindre');
            $table->double('litre', 8, 2);
            $table->integer('prix');
            $table->string('nom', 50)->nullable($value = true);
            $table->string('description', 100)->nullable($value = true);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Moteurs');
    }
}
