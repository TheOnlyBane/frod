<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Vehicule extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Vehicules', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->double('montantFinal', 8, 2);
            $table->bigInteger('moteur_id');
            $table->bigInteger('modele_id');
            $table->bigInteger('couleur_id');
            $table->bigInteger('roue_id');
            $table->bigInteger('accessoire_id');
            $table->bigInteger('utilisateur_id');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Vehicules');
    }
}
