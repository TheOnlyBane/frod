<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Roue extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Roues', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->integer('taille');
            $table->integer('largeur');
            $table->integer('prix');
            $table->bigInteger('pneu_id')->unsigned();
            $table->bigInteger('jante_id')->unsigned();
            $table->foreign('pneu_id')->references('id')->on('Pneus');
            $table->foreign('jante_id')->references('id')->on('Jantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Roues');
    }
}
