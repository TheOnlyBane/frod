<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Accessoire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Accessoires', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nom', 50);
            $table->double('prix', 8, 2);
            $table->bigInteger('couleur_id')->unsigned();
            $table->bigInteger('type_id')->unsigned();
            $table->foreign('couleur_id')->references('id')->on('Couleurs');
            $table->foreign('type_id')->references('id')->on('Types');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Accessoires');
    }
}
