<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Couleur extends Migration
{

    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Couleurs', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('type', 20);
            $table->string('couleur', 50);
            $table->double('prix', 8, 2);
            $table->string('html', 50);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Couleurs');
    }
}
