<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Modele extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Modeles', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('nom', 50);
            $table->double('prix');
            $table->string('description', 100);
            $table->string('transmission',50);
            $table->integer('annee');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Modeles');
    }
}
