<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class VehiculeAccessoire extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Vehicule_Accessoires', function(Blueprint $table){
            $table->bigInteger('vehicule_id')->unsigned();
            $table->foreign('vehicule_id')->references('id')->on('Vehicules');
            $table->bigInteger('accessoire_id')->unsigned();
            $table->foreign('accessoire_id')->references('id')->on('Accessoires');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Vehicule_Accessoires');
    }
}
