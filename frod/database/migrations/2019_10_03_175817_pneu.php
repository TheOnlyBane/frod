<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Pneu extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Pneus', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('type', 50);
            $table->double('prix', 8, 2);
            $table->string('saison', 50);
            $table->bigInteger('marquepneu_id')->unsigned();
            $table->foreign('marquepneu_id')->references('id')->on('Marquepneus');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Pneus');
    }
}
