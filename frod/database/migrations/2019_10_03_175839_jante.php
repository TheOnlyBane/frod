<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jante extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('Jantes', function(Blueprint $table){
            $table->bigIncrements('id');
            $table->string('type', 50);
            $table->double('prix', 8, 2);
            $table->bigInteger('couleur_id')->unsigned();
            $table->bigInteger('marquejante_id')->unsigned();
            $table->foreign('couleur_id')->references('id')->on('Couleurs');
            $table->foreign('marquejante_id')->references('id')->on('MarqueJantes');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('Jantes');
    }
}
