<?php


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MarquesPneuSeeder extends Seeder
{
    public function run()
    {
        DB::table('Marquepneus')->delete();

        DB::table('Marquepneus')->insert([
            'nom' => 'Toyo',
        ]);
        DB::table('Marquepneus')->insert([
            'nom' => 'Pogo',
        ]);
        DB::table('Marquepneus')->insert([
            'nom' => 'Chicoo',
        ]);
        DB::table('Marquepneus')->insert([
            'nom' => 'Micheline',
        ]);
        DB::table('Marquepneus')->insert([
            'nom' => 'Grate',
        ]);
    }
}
