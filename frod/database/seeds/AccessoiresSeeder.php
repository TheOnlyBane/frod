<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Accessoire;
use App\Couleur;
use App\Type;

class AccessoiresSeeder extends Seeder
{
    public function run()
    {
        DB::table('Accessoires')->delete();

        $couleur1 = Couleur::where('couleur','=' ,'Rose Bonbon')->first();
        $couleur2 = Couleur::where('couleur','=' ,'Bleu Royal')->first();
        $couleur3 = Couleur::where('couleur','=' ,'Vert Vomit')->first();
        $type1 = Type::where('nom','=' ,'Tapis')->first();
        $type2 = Type::where('nom','=' ,'Mirroir')->first();

        $accessoire= new Accessoire();
        $accessoire->nom = 'Tapis Frou Frou';
        $accessoire->prix = 49.99;
        $accessoire->couleur()->associate($couleur1);
        $accessoire->type()->associate($type1);
        $accessoire->save();

        $accessoire= new Accessoire();
        $accessoire->nom = 'Tapis Plastic';
        $accessoire->prix = 19.99;
        $accessoire->couleur()->associate($couleur2);
        $accessoire->type()->associate($type1);
        $accessoire->save();

        $accessoire= new Accessoire();
        $accessoire->nom = 'Mirroir Chauffand';
        $accessoire->prix = 49.99;
        $accessoire->couleur()->associate($couleur3);
        $accessoire->type()->associate($type2);
        $accessoire->save();
    }
}
