<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CouleurSeeder extends Seeder
{

    public function run()
    {
        DB::table('Couleurs')->delete();

        DB::table('Couleurs')->insert([
            'couleur' => 'Rouge Sang',
            'type' => 'Chrome',
            'prix' => 1.99,
            'html' => '#a3281a',
        ]);
        DB::table('Couleurs')->insert([
            'couleur' => 'Rose Bonbon',
            'type' => 'Candy',
            'prix' => 2.99,
            'html' => '#ff5291',
        ]);
        DB::table('Couleurs')->insert([
            'couleur' => 'Blanc Doux',
            'type' => 'Standart',
            'prix' => 4.20,
            'html' => '#ffe9a1',
        ]);
        DB::table('Couleurs')->insert([
            'couleur' => 'Bleu Royal',
            'type' => 'Metallique',
            'prix' => 2.99,
            'html' => '#251ac7',
        ]);
        DB::table('Couleurs')->insert([
            'couleur' => 'Vert Vomit',
            'type' => 'Mat',
            'prix' => 1.99,
            'html' => '#79a63a',
        ]);
    }
}
