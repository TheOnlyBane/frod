<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Moteur;
class MoteurSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Moteurs')->delete();

        $moteur = new Moteur();
        $moteur->cylindre = 4;
        $moteur->litre = 1.5;
        $moteur->prix = 1800;
        $moteur->nom = 'TurboMauvais';
        $moteur->description = "Le pire moteur de FROD";
        $moteur->save();

        $moteur = new Moteur();
        $moteur->cylindre = 6;
        $moteur->litre = 3.7;
        $moteur->prix = 2700;
        $moteur->nom = 'FamilyPower';
        $moteur->description = "Le moteur pour le père de famille dans sa van";
        $moteur->save();

        $moteur = new Moteur();
        $moteur->cylindre = 8;
        $moteur->litre = 5.7;
        $moteur->prix = 4000;
        $moteur->nom = 'TurboSuperchargerOfDoom';
        $moteur->description = "Plus vite que la vitesse de ta mère";
        $moteur->save();
    }
}
