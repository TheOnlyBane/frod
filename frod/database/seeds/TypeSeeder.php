<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class TypeSeeder extends Seeder
{

    public function run()
    {
        DB::table('Types')->delete();

        DB::table('Types')->insert([
            'nom' => 'Tapis',
        ]);
        DB::table('Types')->insert([
            'nom' => 'Radio',
        ]);
        DB::table('Types')->insert([
            'nom' => 'Mirroir',
        ]);
    }
}
