<?php


use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Modele;
use App\Moteur;
use App\Roue;
use App\Couleur;
use App\User;
use App\Vehicule;
use App\Accessoire;

class VehiculeSeeder extends Seeder
{
    public function run()
    {
        DB::table('Vehicules')->delete();

        $modele = Modele::where('id','=','1')->first();
        $moteur = Moteur::where('id','=','1')->first();
        $couleur = Couleur::where('id','=','1')->first();
        $roue = Roue::where('id','=','1')->first();
        $accessoire = Accessoire::where('id','=','1')->first();
        $utilisateur = User::where('id','=','1')->first();
        $prixtotal=$modele->prix+$moteur->prix+$couleur->prix+$roue->prix+$accessoire->prix;

        $modele2 = Modele::where('id','=','2')->first();
        $moteur2 = Moteur::where('id','=','2')->first();
        $couleur2 = Couleur::where('id','=','2')->first();
        $roue2 = Roue::where('id','=','2')->first();
        $accessoire2 = Accessoire::where('id','=','2')->first();
        $utilisateur2 = User::where('id','=','2')->first();
        $prixtotal2=$modele2->prix+$moteur2->prix+$couleur2->prix+$roue2->prix+$accessoire2->prix;

        $vehicule= new Vehicule();
        $vehicule->montantFinal = $prixtotal ;
        $vehicule->moteur()->associate($moteur);
        $vehicule->modele()->associate($modele);
        $vehicule->roue()->associate($roue);
        $vehicule->couleur()->associate($couleur);
        $vehicule->accessoire()->associate($accessoire);
        $vehicule->utilisateur()->associate($utilisateur);
        $vehicule->save();



        $vehicule= new Vehicule();
        $vehicule->montantFinal = $prixtotal2 ;
        $vehicule->moteur()->associate($moteur2);
        $vehicule->modele()->associate($modele2);
        $vehicule->roue()->associate($roue2);
        $vehicule->couleur()->associate($couleur2);
        $vehicule->accessoire()->associate($accessoire2);
        $vehicule->utilisateur()->associate($utilisateur2);
        $vehicule->save();
    }
}
