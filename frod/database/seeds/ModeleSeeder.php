<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\modele;

class ModeleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Modeles')->delete();

        $modele = new modele();
        $modele->nom = "Moustang";
        $modele->prix = 42000;
        $modele->description = "Moustachu? Moustang!";
        $modele->transmission = "8 vitesses manuelle";
        $modele->annee = "2019";
        $modele->save();

        $modele = new modele();
        $modele->nom = "Concentrate";
        $modele->prix = 424012;
        $modele->description = "On parlais de quoi?";
        $modele->transmission = "CVT";
        $modele->annee = "2020";
        $modele->save();

        $modele = new modele();
        $modele->nom = "Divorce";
        $modele->prix = 42;
        $modele->description = "Pour les événements importants";
        $modele->transmission = "1 vitesse automatique";
        $modele->annee = "2018";
        $modele->save();
    }
}
