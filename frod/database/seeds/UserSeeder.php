<?php

use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            'name'=>'admin',
            'email'=>'admin@admin.com',
            'password'=>Hash::make('admin123'),
            'isAdmin'=>1
        ]);
        DB::table('users')->insert([
            'name'=>'user',
            'email'=>'user@user.com',
            'password'=>Hash::make('user123'),
            'isAdmin'=>0
        ]);

    }
}
