<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
         $this->call(MoteurSeeder::class);
         $this->call(UserSeeder::class);
         $this->call(ModeleSeeder::class);
         $this->call(TypeSeeder::class);
         $this->call(CouleurSeeder::class);
         $this->call(AccessoiresSeeder::class);
         $this->call(MarquesPneuSeeder::class);
         $this->call(EnsemblePneuSeeder::class);
         $this->call(MarquesJanteSeeder::class);
         $this->call(JantesSeeder::class);
         $this->call(RoueSeeder::class);
         $this->call(VehiculeSeeder::class);
    }
}
