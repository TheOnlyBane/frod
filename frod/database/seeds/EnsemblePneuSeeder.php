<?php
use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

use \App\Marquepneu;
class EnsemblePneuSeeder extends seeder
{

    public function run()
    {
        DB::table('Pneus')->delete();
        $marquepneu= Marquepneu::where('nom','=','Toyo')->first();
        $marquepneu2= Marquepneu::where('nom','=','Pogo')->first();
        $marquepneu3= Marquepneu::where('nom','=','Micheline')->first();

        $pneu=new \App\Pneu();
        $pneu->type='Tout Terrain';
        $pneu->prix='800';
        $pneu->saison='Été';
        $pneu->marquepneu()->associate($marquepneu);
        $pneu->save();

        $pneu=new \App\Pneu();
        $pneu->type='Hors piste';
        $pneu->prix='60';
        $pneu->saison='Hiver';
        $pneu->marquepneu()->associate($marquepneu2);
        $pneu->save();

        $pneu=new \App\Pneu();
        $pneu->type='Pour le plaisire';
        $pneu->prix='1000';
        $pneu->saison='Été';
        $pneu->marquepneu()->associate($marquepneu3);
        $pneu->save();
    }
}
