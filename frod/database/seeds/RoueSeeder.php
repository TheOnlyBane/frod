<?php

use Illuminate\Database\Seeder;
use App\Pneu;
use App\jante;
use App\Roue;

class RoueSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('Roues')->delete();

        $pneu1 = Pneu::where('type', '=', 'Tout Terrain')->first();
        $pneu2 = Pneu::where('type', '=', 'Hors piste')->first();
        $jante1 = jante::where('type', '=', '5 Branches')->first();
        $jante2 = jante::where('type', '=', '6 Branches')->first();

        $roue = new Roue();
        $roue->taille = 16;
        $roue->largeur = 165;
        $roue->prix = $pneu1->prix+$jante1->prix;
        $roue->pneu()->associate($pneu1);
        $roue->jante()->associate($jante1);
        $roue->save();

        $roue = new Roue();
        $roue->taille = 17;
        $roue->largeur = 185;
        $roue->prix = $pneu2->prix+$jante2->prix;
        $roue->pneu()->associate($pneu2);
        $roue->jante()->associate($jante2);
        $roue->save();

        $roue = new Roue();
        $roue->taille = 18;
        $roue->largeur = 205;
        $roue->prix = $pneu1->prix+$jante2->prix;
        $roue->pneu()->associate($pneu1);
        $roue->jante()->associate($jante2);
        $roue->save();


    }
}
