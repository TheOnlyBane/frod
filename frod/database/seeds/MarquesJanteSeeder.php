<?php


use Illuminate\Support\Str;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use App\Marquejante;

class MarquesJanteSeeder extends Seeder
{
    public function run()
    {
        DB::table('Marquejantes')->delete();

        $marquejante = new marquejante();
        $marquejante->nom = "Oxygen";
        $marquejante->save();

        $marquejante = new marquejante();
        $marquejante->nom = "720Triangle";
        $marquejante->save();

        $marquejante = new marquejante();
        $marquejante->nom = "White giraffe";
        $marquejante->save();

        $marquejante = new marquejante();
        $marquejante->nom = "American raging";
        $marquejante->save();

        $marquejante = new marquejante();
        $marquejante->nom = "BS";
        $marquejante->save();
    }
}
