## Table of contents
* [Info Géréral](#Info-Général)
* [Technologies](#technologies-Utilisé)
* [Équipe](#Membre-d'équipe)
* [Prérequis](#Prérequis)
* [Guide Instalation](#Guide-d'installation-du-projet-complet)

#Info Général
Le projet suivant est un site web de concessionnaire automobile fictif appelé Frôd.
Le site web permet de créer différents éléments d'un véhicule tels que les accessoires,
les couleurs, les moteurs, les modèles inclus dans les véhicules ,etc. Le site web à
accès à une base de données afin de stoker en mémoire les différents éléments des véhicules. 
Le site web a été créé comme projet d'équipe au cégep de Drummondville.
Ce projet est présenté à Benoit Desrosiers professeur de web.


#Technologies Utilisé
- PHP 7.3.8 
- XAMPP 3.2.4
- Gitlab.com
- Laravel 6.4.0
- PhpStorm 11.0.3

#Membre d'équipe
- Carl Guillemette
- Jessy Auger
- Anthony Girard
- Christopher Breton

# Prérequis
- Avoir un logiciel d'archivage (Winrar, 7-Zip, etc)
- Une connexion Internet

# Guide d'installation du projet complet

# 1. Aller chercher le projet.
## Il y a deux méthodes possibles pour aller chercher le projet.
1. Méthode par Git
   1. Aller sur le site de Git au https://git-scm.com
   2. Télécharger la derniere version disponible
   3. (Optionel) Vous pouvez aussi cliquer directement sur le lien suivant
https://git-scm.com/download/win
   4.  Installer le logiciel avec les options par défaut
   5.  Créer un dossier à la racine du C. ex.: C:\Projet\
   6.  Ouvrir GitBash et entrer la commande suivante
   `git clone https://gitlab.com/TheOnlyBane/frod.git`
   

1. Méthode par Internet
   1. Aller sur le site https://gitlab.com/TheOnlyBane/frod
   2. Télécharger l'archive en .zip
   3. Créer un dossier à la racine du C. ex.: C:\Projet\
   4. Extraire l'archive dans le dossier créé.

# 2. Installer XAMPP
1. Aller dans le dossier C:\Projet\frod\Installation.
2. Extraire l'archive Installation.zip dans le dossier
3. Installer XAMPP à partir du fichier suivant : *1-XAMPP.exe*
   1. Utiliser les options par défaut

# 3. Installer Composer
1. Aller dans le dossier C:\Projet\frod\Installation.
2. Installer Composer à partir du fichier suivant : *2-Composer.exe*
   1. Utiliser les options par défaut

# 4. Installer NPM
1. Aller dans le dossier C:\Projet\frod\Installation.
2. Installer NPM à partir du fichier suivant : *3-NPM.exe*
   1. Cocher l'option d'ajout dans le path (variables d'environnement)

# 5. Installer les dépendances de Composer
1. Aller dans le dossier C:\Projet\frod\frod\
2. Ouvrir une invite de commande dans ce dossier.
   1. Dans la barre d'adresse de l'explorateur de fichier, entrez : cmd
   2. Sinon:
      1. Dans le menu démarrer, rechercher cmd ou invite de commande
      2. Entrer la commande suivante : `cd C:\Projet\frod\frod`
3. Entrer la commande suivante : `composer install`

# 6. Installer les dépendances de NPM
1. Ouvrir une nouvelle invite de commande dans le dossier C:\Projet\frod\frod\
2. Lancer la commande suivante : `npm install`

# 7. Démarrer les services de Apache et MySQL
1. Dans le panneau de contrôle de XAMPP démarrer les services à l'aide des boutons d'actions
2. Sur la ligne MySQL, cliquer sur [Admin]
3. Cliquer sur le bouton 'nouvelle base de données' en haut à gauche
4. Entrer le nom de la base de données (Par défaut: laravel)
5. Changer l'encodage de texte pour 'utf8mb4_general_ci'
6. Cliquer sur [Créer]

# 8. Préparer le fichier d'environnement
1. Dans la même invite de commande entrer la commande suivante :
`copy .env.example .env`
2. Si vous avez changé le nom de la base de données:
   1. Ouvrir le fichier .env à l'aide d'un éditeur de texte
   2. Changer la ligne  `DB_DATABASE=laravel` pour `DB_DATABASE=Votre Nom`
# 9. Créer la clé d'encryption
1. Dans l'invite de commande entrer la commande suivante:
`php artisan key:generate`

# 9. Faire la migration de la base de données
1. Dans la même invite de commande entrer la commande suivante:
`php artisan migrate`

# 10. Populer la base de données
1. Dans la même invite de commande entrer :
`composer dump-autoload` suivi de `php artisan db:seed`

# 11. Configuration de Apache
1. Dans le panneau de contrôle de XAMPP, sur la ligne de Apache, cliquer sur Config puis Apache (httpd.conf).
2. Changer les lignes
`DocumentRoot "C:/xampp/htdocs"` par `DocumentRoot "C:\Projet\frod\frod\public"`,  `<Directory "C:/xampp/htdocs">` par `<Directory "C:\Projet\frod\frod\public">`.
3. Redémarrer Apache (Stop, Start).